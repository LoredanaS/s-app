#ifndef PWM_H
#define PWM_H

#include<bcm2835.h>
#include<stdint.h>

class PWM
{
private:
    int frequency,duty_cycle;
    int getFrequency(void);
    int getDuty(void);
    void setFrequency(int val);
    void setDuty(int val);
    void reconfig();
public:
    PWM(int freq,int duty);

};

#endif // PWM_H
