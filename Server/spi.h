#ifndef SPI_H
#define SPI_H

#include<bcm2835.h>
#include<stdint.h>
#include<serial.h>

class Spi : public SERIAL
{
public:
    Spi(uint8_t bit_order, uint8_t data_mode, uint16_t clock_divider, uint8_t chip_sel, uint8_t cs_polarity);
    uint8_t transfer(uint8_t send_data);
};

#endif // SPI_H
