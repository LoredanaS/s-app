
// Example program for bcm2835 library
// Blinks a pin on an off every 0.5 secs
//
// After installing bcm2835, you can build this
// with something like:
// gcc -o blink blink.c -l bcm2835
// sudo ./blink
//
// Or you can test it before installing with:
// gcc -o blink -I ../../src ../../src/bcm2835.c blink.c
// sudo ./blink
//
// Author: Mike McCauley
// Copyright (C) 2011 Mike McCauley
// $Id: RF22.h,v 1.21 2012/05/30 01:51:25 mikem Exp $

#include <bcm2835.h>
#include <stdio.h>
#include <pin.h>
#include <pwm.h>
#include "inputpin.h"
#include "outputpin.h"

// Blinks on RPi Plug P1 pin 11 (which is GPIO pin 17)
#define PIN RPI_GPIO_P1_11

// PWM output on RPi Plug P1 pin 12(which is GPIO pin 18)
#define PIN RPI_GPIO_P1_12

// and it is controlled by PWM channel 0
#define PWM_CHANNEL 0
// This controls the max range of the PWM signal
#define RANGE 1024

int main(int argc, char *argv[])
{
    // If you call this, it will not actually access the GPIO
    // Use for testing
    bcm2835_set_debug(1);

    if (!bcm2835_init())
      return 1;

    //Set output pin for led
    //Pin *out = new Pin(17,1,0);

    //Declare a pin to be an input
    //Pin *pin = new Pin(18,0,0);
    //PWM *pwm = new PWM(1.2,20); //set freq and duty
    //pwm->duty_cycle = pwm->getDuty();

    //Pin *pin = new Pin(18,0,0);
    //PWM *pwm = new PWM(1.2,20);

    //At every push button  increment duty cycle by 10%
    //When the duty cycle has reached 100% reconfigure
    //while(1){

        //if(pwm->duty_cycle < 100){
            //if(pin->getPullUp()){
             //int newDuty = pwm->duty_cycle + 10;
             //pwm->setDuty(newDuty);
             //bcm2835_delay(500);
            //}
        //}
        //else{
            //pwm->reconfig();
        //}
    //}

    //Blink
    //while (1)
    //{
    // Turn it on
    //bcm2835_gpio_write(PIN, HIGH);

    // wait a bit
    //bcm2835_delay(500);

    // turn it off
    //bcm2835_gpio_write(PIN, LOW);

    // wait a bit
    //bcm2835_delay(500);
    //}

    // Vary the PWM m/s ratio between 1/RANGE and (RANGE-1)/RANGE
    // over the course of a a few seconds
    //int direction = 1; // 1 is increase, -1 is decrease
    //int data = 1;
    //while (1)
    //{
    //if (data == 1)
        //direction = 1;   // Switch to increasing
    //else if (data == RANGE-1)
        //direction = -1;  // Switch to decreasing
    //data += direction;
    //bcm2835_pwm_set_data(PWM_CHANNEL, data);
    //bcm2835_delay(1);
    //}

    //SPI
    while(1){
    Spi *spi = new Spi(BCM2835_SPI_BIT_ORDER_MSBFIRST, BCM2835_SPI_MODE0, BCM2835_SPI_CLOCK_DIVIDER_65536, BCM2835_SPI_CS0, LOW);
    uint8_t send_data = 0x23;
    printf("Sent to SPI: 0x%02X. Read back from SPI: 0x%02X.\n", send_data, spi->transfer(send_data));
    }
    bcm2835_close();

    //delete in;
    //delete out;
    //delete pwm;
    //delete pin;
    return 0;
}
