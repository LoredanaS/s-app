#include <uart.h>
#include <serial.h>

void run_uart(){

    Uart *uart =new Uart("/dev/ttyAMA0");
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    unsigned char rx_buffer[256];
    int rx_length;
    int count ;

    p_tx_buffer = &tx_buffer[0];
    *p_tx_buffer++ = 'H';
    *p_tx_buffer++ = 'e';
    *p_tx_buffer++ = 'l';
    *p_tx_buffer++ = 'l';
    *p_tx_buffer++ = 'o';

    count = uart->writeFisier(&tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));
    if (count < 0)
       {
           printf("UART TX error\n");
       }

    rx_length = uart->readFisier((void*)rx_buffer);

    if (rx_length < 0)
       {
           printf("UART RX error\n");
       }
       else if (rx_length == 0)
       {
           printf("UART no data\n");
       }
       else
       {
           rx_buffer[rx_length] = '\0';
           printf("%i bytes read : %s\n", rx_length, rx_buffer);
       }
    delete uart;

}
