#ifndef PIN_H
#define PIN_H

#include<bcm2835.h>
#include<stdint.h>

class Pin
{
private:
    int nr,direction,state;
    int get(void);
    void set(int val);
    void changeDir(void);
public:
    Pin(int nr,int direction,int state);

};

#endif // PIN_H
