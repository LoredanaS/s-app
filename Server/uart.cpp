#include <serial.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <uart.h>

Uart::Uart(char *path){
    strcpy(this->path,path);
    uart0_filestream = openFisier(path, O_RDWR | O_NOCTTY | O_NDELAY);//Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
        //ERROR - CAN'T OPEN SERIAL PORT
        printf("Error - Unable to open UART. Ensure it is not in use by another application\n");
    }

    //CONFIGURE THE UART
    //The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
    //	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
    //	CSIZE:- CS5, CS6, CS7, CS8
    //	CLOCAL - Ignore modem status lines
    //	CREAD - Enable receiver
    //	IGNPAR = Ignore characters with parity errors
    //	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
    //	PARENB - Parity enable
    //	PARODD - Odd parity (else even)
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD; //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
}


//destructor
Uart::~Uart(){
  closeFisier(uart0_filestream);
}

void Uart::closeFisier(int descriptor){
    int id=-1;
    id=close(descriptor);
    return id;
}

int Uart::openFisier(char *path, mode_t Mode){
    int id=-1;
    id=open(path,Mode);
    return id;
}

int Uart::readFisier(void *rx_buffer){

    if (uart0_filestream != -1)
    {
        // Read up to 255 characters from the port if they are there
        int rx_length = read(uart0_filestream, (void*)rx_buffer, 255);//Filestream, buffer to store in, number of bytes to read (max)
        /*if (rx_length < 0)
        {
            //An error occured (will occur if there are no bytes)
        }
        else if (rx_length == 0)
        {
            //No data waiting
        }
        else
        {
            //Bytes received
            rx_buffer[rx_length] = '\0';
            //printf("%i bytes read : %s\n", rx_length, rx_buffer);
        }
        */
    }
    return rx_length;
}

int Uart::writeFisier(const void *tx_buffer,int count){

    if (uart0_filestream != -1)
    {
        int tx = write(uart0_filestream, &tx_buffer[0], count);
        //if (tx < 0)
        //{
            //printf("UART TX error\n");
        //}
    }
    return tx;
}
