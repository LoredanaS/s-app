#-------------------------------------------------
#
# Project created by QtCreator 2017-10-13T18:59:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Server
TEMPLATE = app


SOURCES += main.cpp\
    bcm2835_stub.cpp \
    pin.cpp \
    pwm.cpp \
    spi.cpp \
    serial.cpp \
    uart.cpp \
    run_uart.cpp

HEADERS  += \
    bcm2835.h \
    pin.h \
    pwm.h \
    serial.h \
    spi.h \
    uart.h

FORMS    += mainwindow.ui
