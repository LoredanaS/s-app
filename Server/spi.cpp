#include<spi.h>
#include<serial.h>
#include<stdint.h>
#include<bcm2835.h>

Spi::Spi(uint8_t bit_order, uint8_t data_mode, uint16_t clock_divider, uint8_t chip_sel, uint8_t cs_polarity){
    bcm2835_spi_setBitOrder(bit_order);
    bcm2835_spi_setDataMode(data_mode);
    bcm2835_spi_setClockDivider(clock_divider);
    bcm2835_spi_chipSelect(chip_sel);
    bcm2835_spi_setChipSelectPolarity(chip_sel, cs_polarity);
}

uint8_t Spi::transfer(uint8_t send_data){
    return bcm2835_spi_transfer(send_data);
}
