#ifndef UART_H
#define UART_H

#include "serial.h"
#include <unistd.h>
#include <fcntl.h>

class Uart:public SERIAL
{
public:
    Uart(char *path);
    int openFisier(char *path,mode_t Mode);
    int writeFisier(const void *tx_buffer, int count);
    int readFisier(void *rx_buffer);
    int closeFisier(int descriptor);
private:
    char path[20];
    int uart0_filestream;

};

#endif // UART_H
