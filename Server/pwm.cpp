#include "pwm.h"

PWM::PWM(int freq,int duty)
{
    this->frequency = freq;
    this->duty_cycle = duty;
    // Clock divider is set to 16.
    // With a divider of 16 and a RANGE of 1024, in MARKSPACE mode,
    // the pulse repetition frequency will be
    // 1.2MHz/1024 = 1171.875Hz, suitable for driving a DC motor with PWM
    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16);
    bcm2835_pwm_set_mode(PWM_CHANNEL, 1, 1);
    bcm2835_pwm_set_range(PWM_CHANNEL, freq);
}

int PWM::getFrequency(void){
    return bcm2835_gpio_lev(this->frequency);
}
int PWM::getDuty(void){
    return (100 * this->frequency * (bcm2835_gpio_lev(this->duty_cycle)));
}

void PWM::setFrequency(int val){
    bcm2835_gpio_write(this->frequency,val);
}
void PWM::setDuty(int val){
    int value = val * this->duty_cycle / 100;
    bcm2835_gpio_write(this->duty_cycle,value);
}


void PWM::reconfig(void){
    setDuty(0);
    setFrequency(0);
}
