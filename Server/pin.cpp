#include "pin.h"

Pin::Pin(int nr,int direction,int state)
{
    this->nr = nr;
    this->direction = direction;
    if(direction == 1) {
        bcm2835_gpio_fsel(this->nr,BCM2835_GPIO_FSEL_OUTP);
    }
    else{
        bcm2835_gpio_fsel(this->nr,BCM2835_GPIO_FSEL_INPT);
    }
    this->state = state;
}

void Pin::set(int val){
    if(this->direction==0){
        //do nothing
    }
    else{
        bcm2835_gpio_write(this->nr,val);
    }
}

int Pin::get(void){
    return bcm2835_gpio_lev(this->nr);
}

void Pin::changeDir(void){
    if(this->direction==0){
        this->direction = 1;
        bcm2835_gpio_fsel(this->nr,BCM2835_GPIO_FSEL_OUTP);
    } else {
        this->direction = 0;
        bcm2835_gpio_fsel(this->nr,BCM2835_GPIO_FSEL_INPT);
    }
}
